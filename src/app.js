var s = Snap("#app");
var dragableObjs = [];

window.onload = function(){
  draw();
  assignListenners();
};

function draw(){
  //Responsive SVG
  s.attr({ viewBox: "0 0 1024 768"});

  // circle(x,y,r)
  var face = s.circle(150, 150, 100);
  face.attr({
      fill: "#fbddb5",
      stroke: "#000",
      strokeWidth: 10
  });

  var [bndE1,bndE2,eye1,eye2,pupil1,pupil2,s1,s2] = [
    s.circle(100, 150, 70),
    s.circle(200, 150, 70),
    s.circle(100, 150, 60),
    s.circle(200, 150, 60),
    s.circle(100, 150, 20),
    s.circle(200, 150, 20),
    s.circle(110, 140, 5),
    s.circle(210, 140, 5)
  ];
  var shines = s.group(s1,s2);
  shines.attr({
    fill: "#fff",
    opacity: "0.6"
  });

  /*var transform = new Snap.Matrix();
  transform.scale(1,1.5,0,0,0,0);
  shines.transform(transform);*/

  var eyes = s.group(eye1,eye2);
  eyes.attr({
    fill: "#fff"
  });

  var pupils = s.group(pupil1,pupil2);
  pupils.attr({
    fill: "#000",
    stroke: "#0097ff",
    strokeWidth: 5
  });

  var eyeBg = s.group(bndE1,bndE2);

  var eyesSkin = eyes.clone();
  eyesSkin.attr({
    fill: "#fbddb5"
  });

  var eyeballs = s.group(eyes,pupils,shines);
  var fullEyes = s.group(eyeBg,eyesSkin,eyeballs);

  //Mascaras
  var eyesMask = eyes.clone();
  eyeballs.attr({
    mask: eyesMask
  });

  //Animaciones
  /*eyesMask.animate({ transform: 't100,0'},1000,function(){
    eyesMask.animate({ transform: 't0,0'},1000)
  });*/
  fullEyes.animateEyes = function(){
    eyesMask.animate({ transform: 's1,0'},100,function(){
        var time = 400 * Math.random();
        setTimeout(function(){
          //random pupils color
          pupils.attr({
            stroke: '#'+Math.floor(Math.random()*16777215).toString(16)
          });
          eyesMask.animate({ transform: 's1,1'},100);
        },time);
    });
  };

  //Eventos
  fullEyes.click(function(){
    this.animateEyes();
  }).touchstart(function(){
    this.animateEyes();
  });

  var character = s.group(face,fullEyes);
  dragableObjs.push(character);
  //Responsive Drag (transformation matrix modified)
  //isRDraggable(eyeballs);

  //Load SVG's
  var mascot = Snap.load('./svg/mascot.svg',function(fragment){
    var mascot = fragment.select("g");

    var t = new Snap.Matrix()
    t.translate(0, 250);
    mascot.transform(t);

    s.append(mascot);
    isDraggable(mascot.select("#symbol"));

    mascot.exclaim = function(){

    var upperH = mascot.select("#upper-head");

    var t = new Snap.Matrix()
    t.rotate(40);
    t.translate(80, -90);

    upperH.animate({transform: t}, 200, function(){

      var t = new Snap.Matrix()
      t.translate(0, 0);
      t.rotate(0);
      upperH.animate({transform: t}, 300,false);
    });

      var symbol = this.select("#symbol");
      symbol.animate({
        fill:'#ff7100',
        transform:'s2,2',
        opacity: 1
      },100,function(){
        symbol.animate({
          fill:'#848383',
          transform: 's1,1',
          opacity: 0.2
        },100);
      });
    }

    mascot.click(function(){
      this.exclaim();
    });
  });
}

function assignListenners(){
  window.onresize = function(){
    //s.attr({ viewBox: "0 0 "+window.innerWidth+" "+window.innerHeight });
  }

  listenDrags(dragableObjs);
}

//Cool Examples
//http://codepen.io/jjperezaguinaga/pen/yuBdq
//http://codepen.io/sol0mka/pen/ogOYJj/
//Otras Librerías
//http://mojs.io/tutorials/shape/
